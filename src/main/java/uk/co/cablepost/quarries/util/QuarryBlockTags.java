package uk.co.cablepost.quarries.util;

import net.minecraft.block.Block;
import net.minecraft.tag.TagKey;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import uk.co.cablepost.quarries.QuarriesMod;

public class QuarryBlockTags {
    public static final TagKey<Block> QUARRY_MINEABLE = register("mineable/quarry");

    private QuarryBlockTags() {
    }

    private static TagKey<Block> register(String id) {
        return TagKey.of(Registry.BLOCK_KEY, new Identifier(QuarriesMod.MOD_ID, id));
    }
}
