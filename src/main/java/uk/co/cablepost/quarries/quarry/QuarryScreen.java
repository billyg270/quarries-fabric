package uk.co.cablepost.quarries.quarry;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.quarries.QuarriesMod;

public class QuarryScreen extends HandledScreen<QuarryScreenHandler> {
    private static final Identifier TEXTURE = new Identifier(QuarriesMod.MOD_ID, "textures/gui/container/quarry.png");

    public QuarryScreen(QuarryScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundWidth = 176;
        this.backgroundHeight = 220;
        this.playerInventoryTitleY = 126;
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);

        Text drillHeadText = Text.of("Drill head");
        float drillHeadX = 52f - ((float)textRenderer.getWidth(drillHeadText) / 2f);
        this.textRenderer.draw(matrices, drillHeadText, x + drillHeadX, y + 20f, 4210752);

        Text fuelText = Text.of("Fuel");
        float fuelX = 125f - ((float)textRenderer.getWidth(fuelText) / 2f);
        this.textRenderer.draw(matrices, fuelText, x + fuelX, y + 20f, 4210752);

        this.textRenderer.draw(matrices, Text.of("Output"), x + (float)this.playerInventoryTitleX, y + 56f, 4210752);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        super.render(matrices, mouseX, mouseY, delta);
        this.drawMouseoverTooltip(matrices, mouseX, mouseY);
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }
}