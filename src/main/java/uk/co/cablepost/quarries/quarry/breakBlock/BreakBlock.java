package uk.co.cablepost.quarries.quarry.breakBlock;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerChunkManager;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import uk.co.cablepost.quarries.quarry.QuarryBlockEntity;
import uk.co.cablepost.quarries.util.GetCanMoveResp;

public class BreakBlock {
    public BlockPos targetBlockBreakPos;

    private BlockState breakState;
    private ItemStack breakStack = ItemStack.EMPTY;
    private int breakProgress = 0;
    private FakePlayerEntity fakePlayerEntity;

    private QuarryBlockEntity quarryBlockEntity;

    public BreakBlock(QuarryBlockEntity quarryBlockEntity){
        this.quarryBlockEntity = quarryBlockEntity;
    }

    private BlockPos getBlockPos(){
        return quarryBlockEntity.getPos();
    }

    private World getWorld(){
        return quarryBlockEntity.getWorld();
    }

    public boolean onTick(){
        if (!isBreaking()) {
            startBreak();
            return false;
        }

        BlockState currentBreakState = getWorld().getBlockState(getBreakPos());
        BlockState lastBreakState = getBreakState();

        if (lastBreakState == null){
            startBreak();
        }

        BlockState newBreakState = getBreakState();
        if (newBreakState == null || (!getBreakStack().equals(getTool()) || !newBreakState.equals(lastBreakState) || newBreakState.getHardness(getWorld(), getBlockPos()) < 0)) {
            cancelBreak();
        } else if (getBreakProgress() >= getBreakTime()) {
            finishBreak();
            return true;
        }

        ItemStack tool = getTool();

        if(!breakStack.isItemEqual(tool) && (!breakStack.isEmpty() || !tool.isEmpty())){
            cancelBreak();
            return false;
        }

        continueBreak();
        return false;
    }

    public void updateCracksAndComparators() {
        World world = getWorld();
        BlockPos blockPos = getBlockPos();

        if(world == null) return;
        world.updateComparators(blockPos, world.getBlockState(blockPos).getBlock());

        int breakPercentage = getBreakPercentage();
        int crackProgress = breakPercentage <= 0 || breakPercentage >= 100 ? -1 : breakPercentage / 10;

        world.setBlockBreakingInfo(getFakePlayer().getId(), getBreakPos(), crackProgress);
    }

    public static int getBreakPercentage(int breakProgress, int breakTime) {
        if (breakTime > 0) {
            float div = ((float) breakProgress / (float) breakTime);
            return Math.min((int) (div * 100), 100);
        } else return 0;
    }

    public ItemStack getBreakStack() {
        return breakStack;
    }

    public void setBreakStack(ItemStack stack) {
        this.breakStack = stack;
    }

    public BlockState getBreakState() {
        return breakState;
    }

    public void setBreakState(BlockState state) {
        this.breakState = state;
    }

    public int getBreakProgress() {
        return breakProgress;
    }

    public void setBreakProgress(int breakProgress) {
        this.breakProgress = breakProgress;
    }

    public void incrementBreakProgress() {
        this.breakProgress++;
        this.updateCracksAndComparators();
    }

    public void resetBreakProgress() {
        this.breakProgress = 0;
    }

    public int getBreakTime() {
        BlockState breakState = this.getBreakState();
        ItemStack stack = this.getBreakStack();
        if (breakState == null) return 0;
        float baseTime = this.calcBlockBreakingTime();
        float itemMultiplier = stack.getMiningSpeedMultiplier(breakState);
        int level = EnchantmentHelper.getLevel(Enchantments.EFFICIENCY, stack);
        if (level > 0 && itemMultiplier > 1.0f) {
            itemMultiplier += (level * level + 1);
        }
        float time = (baseTime / itemMultiplier) * getBreakTimeMultiplier();
        return (int) time;
    }

    public float getBreakTimeMultiplier() {
        return 0.8f;
    }

    public boolean isToolEffective() {
        BlockState breakState = this.getBreakState();
        if (breakState == null) return false;
        return !breakState.isToolRequired() || this.getTool().isSuitableFor(breakState);
    }

    public float calcBlockBreakingTime() {
        BlockState breakState = this.getBreakState();
        if (breakState == null) return 0.0F;
        float hardness = breakState.getHardness(getWorld(), getBreakPos());
        if (hardness == -1.0F) {
            return 0.0F;
        } else {
            int multiplier = isToolEffective() ? 30 : 100;
            return hardness * (float) multiplier;
        }
    }

    public void startBreak() {
        BlockState toBreak = getWorld().getBlockState(getBreakPos());
        ItemStack tool = this.getTool();

        this.setBreakState(toBreak);
        this.setBreakStack(tool);
        this.incrementBreakProgress();

        quarryBlockEntity.markDirty();
    }

    public void cancelBreak() {
        this.setBreakState(null);
        this.setBreakStack(ItemStack.EMPTY);
        this.resetBreakProgress();
        this.updateCracksAndComparators();

        quarryBlockEntity.markDirty();
    }

    public void finishBreak() {
        this.breakBlock();
        this.cancelBreak();

        quarryBlockEntity.markDirty();
    }

    public boolean isBreaking() {
        return getBreakProgress() > 0;
    }

    public BlockPos getBreakPos() {
        return quarryBlockEntity.getWorldPosFromRelative(
                (int)Math.floor(quarryBlockEntity.drillHeadOffsetX),
                (int)Math.floor(quarryBlockEntity.drillHeadOffsetY),
                (int)Math.floor(quarryBlockEntity.drillHeadOffsetZ)
        );
    }

    public void breakBlock() {
        World world = getWorld();
        if (world != null && !world.isClient()) {
            BlockState breakState = this.getBreakState();
            PlayerEntity fakePlayer = this.getFakePlayer();
            BlockEntity blockEntity = breakState.hasBlockEntity() ? world.getBlockEntity(getBreakPos()) : null;
            fakePlayer.setStackInHand(Hand.MAIN_HAND, getBreakStack());
            if (getTool().getItem().canMine(breakState, world, getBreakPos(), fakePlayer) && isToolEffective()) {
                //Block.dropStacks(breakState, world, getBlockPos(), blockEntity, fakePlayer, getTool());

                if (world instanceof ServerWorld) {
                    Block.getDroppedStacks(breakState, (ServerWorld)world, getBreakPos(), blockEntity, fakePlayer, getTool()).forEach((stack) -> {
                        ItemStack leftStack = quarryBlockEntity.addToOutput(stack);
                        if(!leftStack.isEmpty()){
                            Block.dropStack(world, getBreakPos(), leftStack);
                        }
                    });
                    breakState.onStacksDropped((ServerWorld)world, getBreakPos(), getTool());
                }
            }
            getTool().getItem().postMine(getTool(), world, breakState, getBreakPos(), fakePlayer);
            world.breakBlock(getBreakPos(), false);
        }
    }

    public void continueBreak() {
        this.incrementBreakProgress();
        quarryBlockEntity.markDirty();
    }

    public ItemStack getTool() {
        return quarryBlockEntity.getStack(0);
    }

    public PlayerEntity getFakePlayer() {
        if (fakePlayerEntity == null) fakePlayerEntity = new FakePlayerEntity(getWorld(), getBlockPos());
        return fakePlayerEntity;
    }

    public int getBreakPercentage() {
        if(breakStack == null || breakState == null) return -1;
        return getBreakPercentage(this.getBreakProgress(), this.getBreakTime());
    }
}
