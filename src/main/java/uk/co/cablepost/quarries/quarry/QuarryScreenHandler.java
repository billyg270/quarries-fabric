package uk.co.cablepost.quarries.quarry;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import uk.co.cablepost.quarries.QuarriesMod;

public class QuarryScreenHandler extends ScreenHandler {
    public Inventory inventory;
    public PropertyDelegate propertyDelegate;

    public QuarryScreenHandler(int syncId, PlayerInventory playerInventory) {
        this(syncId, playerInventory, new SimpleInventory(QuarryBlockEntity.INVENTORY_SIZE), new ArrayPropertyDelegate(QuarryBlockEntity.PROPERTY_DELEGATE_SIZE));
    }

    public QuarryScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(QuarriesMod.QUARRY_SCREEN_HANDLER, syncId);
        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        this.addProperties(this.propertyDelegate);

        //some inventories do custom logic when a player opens it.
        inventory.onOpen(playerInventory.player);

        //drill
        for (int i = QuarryBlockEntity.DRILL_INVENTORY_SLOT_START; i <= QuarryBlockEntity.DRILL_INVENTORY_SLOT_END; i++) {
            this.addSlot(new Slot(inventory, i, 44 + i * 18, 32));
        }

        //fuel
        for (int i = QuarryBlockEntity.FUEL_INVENTORY_SLOT_START; i <= QuarryBlockEntity.FUEL_INVENTORY_SLOT_END; i++) {
            this.addSlot(new Slot(inventory, i, 98 + (i - QuarryBlockEntity.FUEL_INVENTORY_SLOT_START) * 18, 32));
        }

        //output
        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(inventory, QuarryBlockEntity.OUTPUT_INVENTORY_SLOT_START + (l + m * 9), 8 + l * 18, 68 + m * 18));
            }
        }

        //The player inventory
        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 138 + m * 18));
            }
        }

        //The player Hotbar
        for (int m = 0; m < 9; ++m) {
            this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 196));
        }
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return true;
    }

    @Override
    public ItemStack transferSlot(PlayerEntity player, int invSlot) {
        ItemStack newStack = ItemStack.EMPTY;
        Slot slot = this.slots.get(invSlot);
        if (slot.hasStack()) {
            ItemStack originalStack = slot.getStack();
            newStack = originalStack.copy();
            if (invSlot < this.inventory.size()) {
                if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
                return ItemStack.EMPTY;
            }

            if (originalStack.isEmpty()) {
                slot.setStack(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }
        }

        return newStack;
    }
}
