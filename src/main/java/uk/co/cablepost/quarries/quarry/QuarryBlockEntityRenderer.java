package uk.co.cablepost.quarries.quarry;

import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.WorldRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3f;
import net.minecraft.world.World;

public class QuarryBlockEntityRenderer <T extends QuarryBlockEntity> implements BlockEntityRenderer<T> {
    public QuarryBlockEntityRenderer(BlockEntityRendererFactory.Context context) {
        super();
    }

    @Override
    public void render(T entity, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {
        matrices.push();

        BlockState state = entity.getCachedState();

        matrices.translate(0.5f, 0.33f, 0.5f);

        Direction facing = state.get(QuarryBlock.FACING);

        World world = entity.getWorld();

        int worldLight = 0;
        if(world != null) {
            worldLight = WorldRenderer.getLightmapCoordinates(world, entity.getPos().offset(facing.getOpposite()));
        }

        if(facing == Direction.NORTH) {
            matrices.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(0));
        }
        if(facing == Direction.EAST) {
            matrices.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(270));
        }
        if(facing == Direction.SOUTH) {
            matrices.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(180));
        }
        if(facing == Direction.WEST) {
            matrices.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(90));
        }

        ItemStack armStack = new ItemStack(Items.SMOOTH_STONE);
        ItemStack armDownStack = new ItemStack(Items.LIGHT_GRAY_CONCRETE);
        ItemStack armJointStack = new ItemStack(Items.OXIDIZED_COPPER);
        ItemStack drillHeadStack = entity.validDrillSelected() ? entity.getStack(0) : ItemStack.EMPTY;

        float startOffset = QuarryBlockEntity.DRILL_RENDER_START_OFFSET;

        matrices.translate(0f, -0.1f, startOffset);

        //entity.drillHeadOffsetX = 0;

        float baseAngel = (float)Math.atan2(entity.drillHeadOffsetZ - startOffset, -entity.drillHeadOffsetX) - (float)Math.PI/2f;
        if(entity.drillHeadOffsetZ == 0){
            baseAngel = -3.14f;
        }
        matrices.multiply(Vec3f.POSITIVE_Y.getRadialQuaternion(baseAngel));

        //matrices.translate(entity.drillHeadOffsetX, entity.drillHeadOffsetY, entity.drillHeadOffsetZ - startOffset);

        //--- Jointed arm ---
        float jointArmPartLength = QuarryBlockEntity.DRILL_ARM_PART_LENGTH;

        matrices.multiply(Vec3f.POSITIVE_Y.getRadialQuaternion(entity.armJointAngle));

        matrices.translate(0f, 0f, jointArmPartLength / 2f);

        matrices.scale(1.5f, 1.5f, (jointArmPartLength * 4f));
        MinecraftClient.getInstance().getItemRenderer().renderItem(armStack, ModelTransformation.Mode.GROUND, worldLight, overlay, matrices, vertexConsumers, 0);
        matrices.scale(1f / 1.5f, 1f / 1.5f, 1f / (jointArmPartLength * 4f));

        matrices.translate(0f, 0f, jointArmPartLength / 2f);

        //joint

        matrices.multiply(Vec3f.POSITIVE_Y.getRadialQuaternion(-entity.armJointAngle));
        matrices.translate(0f, -0.1f, 0f);
        matrices.scale(2f, 2f, 2f);
        MinecraftClient.getInstance().getItemRenderer().renderItem(armJointStack, ModelTransformation.Mode.GROUND, worldLight, overlay, matrices, vertexConsumers, 0);
        matrices.scale(1f/2f, 1f/2f, 1f/2f);
        matrices.translate(0f, 0.1f, 0f);
        matrices.multiply(Vec3f.POSITIVE_Y.getRadialQuaternion(-entity.armJointAngle));

        //forearm

        matrices.translate(0f, -0.01f, 0f);//prevent z-fighting between arm parts

        //matrices.multiply(Vec3f.POSITIVE_Y.getRadialQuaternion(-(entity.armJointAngle * 2f)));//Uncomment if remove rotations on joint

        matrices.translate(0f, 0f, jointArmPartLength/2f);

        matrices.scale(1.5f, 1.5f, (jointArmPartLength * 4f));
        MinecraftClient.getInstance().getItemRenderer().renderItem(armStack, ModelTransformation.Mode.GROUND, worldLight, overlay, matrices, vertexConsumers, 0);
        matrices.scale(1f/1.5f, 1f/1.5f, 1f/(jointArmPartLength * 4f));

        matrices.translate(0f, 0f, jointArmPartLength/2f);

        //joint

        matrices.translate(0f, -0.1f, 0f);
        matrices.scale(2f, 2f, 2f);
        MinecraftClient.getInstance().getItemRenderer().renderItem(armJointStack, ModelTransformation.Mode.GROUND, worldLight, overlay, matrices, vertexConsumers, 0);
        matrices.scale(1f/2f, 1f/2f, 1f/2f);
        matrices.translate(0f, 0.1f, 0f);


        //-------------------

        /*
        //--- Stretchy single arm ---
        matrices.translate(0f, 0f, dis/2f);

        matrices.scale(1.5f, 1.5f, (dis * 4f));
        MinecraftClient.getInstance().getItemRenderer().renderItem(armStack, ModelTransformation.Mode.GROUND, worldLight, overlay, matrices, vertexConsumers, 0);
        matrices.scale(1f/1.5f, 1f/1.5f, 1f/(dis * 4f));

        matrices.translate(0f, 0f, dis/2f);
        //----------------------------
        */

        float disDown = Math.abs(entity.drillHeadOffsetY);

        //matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(180));

        if(entity.getCachedState().get(QuarryBlock.ENABLED) && state.get(QuarryBlock.LIT)){
            entity.drillRot += tickDelta * 15f;
        }

        matrices.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(entity.drillRot));

        if(disDown > 0f) {
            matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(90));

            matrices.translate(0f, -0.2f, disDown/2f - 0.5f);

            matrices.scale(1f, 1f, (disDown * 4f));
            MinecraftClient.getInstance().getItemRenderer().renderItem(armDownStack, ModelTransformation.Mode.GROUND, worldLight, overlay, matrices, vertexConsumers, 0);
            matrices.scale(1f, 1f, 1f/(disDown * 4f));

            matrices.translate(0f, 0.3f, -disDown/2f + 0.5f);

            matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(-90));

        }
        else{
            matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(90));
            matrices.translate(0f, -0.5f, -0.3f);
        }

        matrices.translate(0f, -disDown + 0.7f, -0.12f);

        matrices.scale(2f, 2f, 2f);

        matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(180));
        MinecraftClient.getInstance().getItemRenderer().renderItem(drillHeadStack, ModelTransformation.Mode.GROUND, worldLight, overlay, matrices, vertexConsumers, 0);

        matrices.pop();
    }

    public boolean rendersOutsideBoundingBox(T blockEntity) {
        return true;
    }

    public int getRenderDistance(){
        return 512;
    }
}
