package uk.co.cablepost.quarries.quarry;

import net.minecraft.block.AbstractFurnaceBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.quarries.QuarriesMod;
import uk.co.cablepost.quarries.drill.DrillItem;
import uk.co.cablepost.quarries.quarry.breakBlock.BreakBlock;
import uk.co.cablepost.quarries.util.GetCanMoveResp;

import java.util.Objects;
import java.util.stream.IntStream;

public class QuarryBlockEntity extends BlockEntity implements SidedInventory, NamedScreenHandlerFactory {
    public static int DRILL_INVENTORY_SIZE = 1;
    public static int DRILL_INVENTORY_SLOT_START = 0;//0
    public static int DRILL_INVENTORY_SLOT_END = 0;//0

    public static int FUEL_INVENTORY_SIZE = 3;
    public static int FUEL_INVENTORY_SLOT_START = DRILL_INVENTORY_SLOT_END + 1;//1
    public static int FUEL_INVENTORY_SLOT_END = FUEL_INVENTORY_SLOT_START + FUEL_INVENTORY_SIZE - 1;//3

    public static int OUTPUT_INVENTORY_SIZE = 27;
    public static int OUTPUT_INVENTORY_SLOT_START = FUEL_INVENTORY_SLOT_END + 1;//4
    public static int OUTPUT_INVENTORY_SLOT_END = OUTPUT_INVENTORY_SLOT_START + OUTPUT_INVENTORY_SIZE - 1;//30

    public static int INVENTORY_SIZE = DRILL_INVENTORY_SIZE + FUEL_INVENTORY_SIZE + OUTPUT_INVENTORY_SIZE;

    protected DefaultedList<ItemStack> inventory = DefaultedList.ofSize(INVENTORY_SIZE, ItemStack.EMPTY);

    public static float MINE_AREA_SIZE = 7;
    public static float DRILL_MOVEMENT_SPEED = 0.1f;
    public float drillMovementSpeed = 1f;

    public float drillHeadOffsetX;//+ is left
    public float drillHeadOffsetY;//+ is up
    public float drillHeadOffsetZ;//+ is back

    public float targetDrillHeadOffsetX;//+ is left
    public float targetDrillHeadOffsetY;//+ is up
    public float targetDrillHeadOffsetZ;//+ is back

    private BreakBlock breakBlock;

    public float drillRot = 0f;

    public float armJointAngle = -1.45f;
    public float targetArmJointAngle = -1.39626f;

    public static float DRILL_RENDER_START_OFFSET = 1f;
    public static float DRILL_ARM_PART_LENGTH = 3.4f;

    public int burnTime;//how many ticks the flame from current fuel has left

    public boolean canMove;


    public QuarryBlockEntity(BlockPos pos, BlockState state) {
        super(QuarriesMod.QUARRY_BLOCK_ENTITY, pos, state);

        drillHeadOffsetX = 0.1f;
        targetDrillHeadOffsetX = 3;

        drillHeadOffsetY = -0.1f;
        targetDrillHeadOffsetY = -1;


        drillHeadOffsetZ = 1;
        targetDrillHeadOffsetZ = 1;
    }


    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);

        inventory = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
        Inventories.readNbt(nbt, inventory);

        drillHeadOffsetX = nbt.getFloat("drillHeadOffsetX");
        drillHeadOffsetY = nbt.getFloat("drillHeadOffsetY");
        drillHeadOffsetZ = nbt.getFloat("drillHeadOffsetZ");

        targetDrillHeadOffsetX = nbt.getFloat("targetDrillHeadOffsetX");
        targetDrillHeadOffsetY = nbt.getFloat("targetDrillHeadOffsetY");
        targetDrillHeadOffsetZ = nbt.getFloat("targetDrillHeadOffsetZ");

        armJointAngle = nbt.getFloat("armJointAngle");
        targetArmJointAngle = nbt.getFloat("targetArmJointAngle");

        burnTime = nbt.getInt("burnTime");

        canMove = nbt.getBoolean("canMoveClient");
    }

    @Override
    protected void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);

        Inventories.writeNbt(nbt, inventory);

        nbt.putFloat("drillHeadOffsetX", drillHeadOffsetX);
        nbt.putFloat("drillHeadOffsetY", drillHeadOffsetY);
        nbt.putFloat("drillHeadOffsetZ", drillHeadOffsetZ);

        nbt.putFloat("targetDrillHeadOffsetX", targetDrillHeadOffsetX);
        nbt.putFloat("targetDrillHeadOffsetY", targetDrillHeadOffsetY);
        nbt.putFloat("targetDrillHeadOffsetZ", targetDrillHeadOffsetZ);

        nbt.putFloat("armJointAngle", armJointAngle);
        nbt.putFloat("targetArmJointAngle", targetArmJointAngle);

        nbt.putInt("burnTime", burnTime);

        nbt.putBoolean("canMoveClient", canMove);
    }

    public static int PROPERTY_DELEGATE_SIZE = 0;

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            return 0;
        }

        @Override
        public void set(int index, int value) {
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !Objects.requireNonNull(this.getWorld()).isClient();

        super.markDirty();

        if (serverSide) {
            assert world != null;
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }

    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt() {
        return createNbt();
    }

    public static void clientTick(World world, BlockPos pos, BlockState state, QuarryBlockEntity blockEntity) {
        if(blockEntity.canMove) {
            blockEntity.updateDrillMovementSpeed();
            blockEntity.moveDrillTowardsTarget();
        }
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, QuarryBlockEntity blockEntity) {
        BlockState blockState = blockEntity.getCachedState();

        boolean canMoveBefore = blockEntity.canMove;
        blockEntity.canMove = true;

        if(!blockState.get(QuarryBlock.ENABLED)){
            blockEntity.canMove = false;
        }

        if(blockEntity.outputFull()){
            blockEntity.canMove = false;
        }

        if(!blockEntity.validDrillSelected()){
            blockEntity.canMove = false;
        }

        blockEntity.updateFuel(state);

        if(blockEntity.burnTime <= 0){
            blockEntity.canMove = false;
        }

        if(blockEntity.canMove) {
            blockEntity.updateDrillMovementSpeed();
            if (blockEntity.moveDrillTowardsTarget()) {
                if (blockEntity.mine()) {
                    blockEntity.updateNextTargetDrillPosition();
                    blockEntity.updateNextTargetDrillPositionRaycast();

                    blockEntity.markDirty();
                    return;
                }
            }
        }

        if(blockEntity.canMove != canMoveBefore){
            blockEntity.markDirty();
        }
    }

    public void updateDrillMovementSpeed(){
        ItemStack stack = getStack(0);
        if(stack.isEmpty()){
            drillMovementSpeed = DRILL_MOVEMENT_SPEED;
            return;
        }

        float itemMultiplier = stack.getMiningSpeedMultiplier(Blocks.STONE.getDefaultState());
        int level = EnchantmentHelper.getLevel(Enchantments.EFFICIENCY, stack);
        if (level > 0 && itemMultiplier > 1.0f) {
            itemMultiplier += level * 0.7f;
        }
        drillMovementSpeed = DRILL_MOVEMENT_SPEED * itemMultiplier * 0.2f;
    }

    public void updateFuel(BlockState blockState){
        int burnTimeBefore = burnTime;

        if(burnTime > 0){
            burnTime--;
        }

        if(burnTime <= 0){
            for(int i = FUEL_INVENTORY_SLOT_START; i <= FUEL_INVENTORY_SLOT_END; i++){
                ItemStack fuelStack = getStack(i);
                int newBurnTime = getFuelTime(fuelStack) * 2;
                if(newBurnTime <= 0){
                    continue;
                }

                burnTime = newBurnTime;

                Item fuelItem = fuelStack.getItem();
                fuelStack.decrement(1);
                if (fuelStack.isEmpty()) {
                    Item fuelItemRemainder = fuelItem.getRecipeRemainder();
                    setStack(i, fuelItemRemainder == null ? ItemStack.EMPTY : new ItemStack(fuelItemRemainder));
                }

                break;
            }
        }

        if(burnTimeBefore != burnTime){
            blockState = blockState.with(AbstractFurnaceBlock.LIT, burnTime > 0);
            if(world != null) {
                world.setBlockState(pos, blockState, 3);
            }
        }
    }

    protected int getFuelTime(ItemStack fuel) {
        if (fuel.isEmpty()) {
            return 0;
        } else {
            Item item = fuel.getItem();
            return AbstractFurnaceBlockEntity.createFuelTimeMap().getOrDefault(item, 0);
        }
    }

    public boolean validDrillSelected(){
        return !getStack(0).isEmpty() && getStack(0).getItem() instanceof DrillItem;
    }

    public boolean mine(){
        if(breakBlock == null){
            breakBlock = new BreakBlock(this);
        }

        BlockPos drillBlockPos = getWorldPosFromRelative(
                Math.round(targetDrillHeadOffsetX),
                Math.round(targetDrillHeadOffsetY),
                Math.round(targetDrillHeadOffsetZ)
        );

        if(shouldMine(drillBlockPos)){
            return breakBlock.onTick();
        }
        else{
            if(breakBlock.getBreakState() != null){
                breakBlock.cancelBreak();
            }
            return true;
        }
    }

    public BlockPos getWorldPosFromRelative(int x, int y, int z){
        Direction facing = getCachedState().get(QuarryBlock.FACING);

        if(facing == Direction.SOUTH){
            x = -x;
            z = -z;
        }

        if(facing == Direction.EAST){
            int tmp = z;
            z = x;
            x = -tmp;
        }

        if(facing == Direction.WEST){
            int tmp = x;
            x = z;
            z = -tmp;
        }

        BlockPos pos = getPos();
        return new BlockPos(pos.getX() + x, pos.getY() + y, pos.getZ() + z);
    }

    public void setRelativeDrillTargetFromWorldBlockPos(BlockPos blockPos){
        Direction facing = getCachedState().get(QuarryBlock.FACING);

        int x = blockPos.getX() - pos.getX();
        int y = blockPos.getY() - pos.getY();
        int z = blockPos.getZ() - pos.getZ();

        if(facing == Direction.SOUTH){
            x = -x;
            z = -z;
        }

        if(facing == Direction.EAST){
            int tmp = z;
            z = -x;
            x = -tmp;
        }

        if(facing == Direction.WEST){
            int tmp = x;
            x = z;
            z = -tmp;
        }

        targetDrillHeadOffsetX = x;
        targetDrillHeadOffsetY = y;
        targetDrillHeadOffsetZ = z;
    }

    public boolean shouldMine(BlockPos blockPos){
        if(world == null){
            return false;
        }

        if(blockPos.getX() == 0 && blockPos.getY() == 0 && blockPos.getZ() == 0){
            return false;
        }

        BlockState blockState = world.getBlockState(blockPos);

        if(blockState.isAir()){
            return false;
        }

        Block block = blockState.getBlock();

        return block != Blocks.WATER && block != Blocks.LAVA && block != Blocks.CAVE_AIR;
    }

    public boolean moveDrillTowardsTarget(){
        if((drillHeadOffsetX != targetDrillHeadOffsetX || drillHeadOffsetZ != targetDrillHeadOffsetZ) && drillHeadOffsetY < Math.min(targetDrillHeadOffsetY + 1.3f, -1f)){
            //raise drill by 1 block while moving across
            drillHeadOffsetY += drillMovementSpeed * 1.5f;
            return false;
        }

        if(!moveArmJointAngleTowardsTarget(true)){
            return false;
        }

        if(drillHeadOffsetY < targetDrillHeadOffsetY){//if too low, come up first
            moveDrillTowardsTargetY();
            return false;
        }

        boolean toRet = true;

        toRet = moveDrillTowardsTargetX() && toRet;
        toRet = moveDrillTowardsTargetZ() && toRet;

        if(toRet && drillHeadOffsetY > targetDrillHeadOffsetY){//only go down when at x,z position (thus toRet must be true)
            toRet = moveDrillTowardsTargetY() && toRet;
        }

        return toRet;
    }

    public boolean moveArmJointAngleTowardsTarget(boolean looseMatch){
        updateTargetArmJointAngle();

        float rotateSpeed = drillMovementSpeed * 0.15f;

        if(armJointAngle > targetArmJointAngle){
            armJointAngle -= rotateSpeed;
        }
        if(armJointAngle < targetArmJointAngle){
            armJointAngle += rotateSpeed;
        }

        if(Math.abs(armJointAngle - targetArmJointAngle) < rotateSpeed){
            armJointAngle = targetArmJointAngle;
            return true;
        }

        if(looseMatch){
            return Math.abs(armJointAngle - targetArmJointAngle) < 0.35f;
        }

        return false;
    }

    public void updateTargetArmJointAngle(){
        float dis = (float)Math.sqrt(Math.pow(targetDrillHeadOffsetX, 2) + Math.pow(targetDrillHeadOffsetZ - DRILL_RENDER_START_OFFSET, 2));
        //float targetBaseAngle = (float)Math.atan2(targetDrillHeadOffsetZ - DRILL_RENDER_START_OFFSET, -targetDrillHeadOffsetX) - (float)Math.PI/2f;
        float baseAngle = (float)Math.atan2(drillHeadOffsetZ - DRILL_RENDER_START_OFFSET, -drillHeadOffsetX) - (float)Math.PI/2f;

        float jointDirectionMul = targetArmJointAngle > 0f ? 1f : -1f ;

        //if(Math.abs(targetDrillHeadOffsetX) > MINE_AREA_SIZE / 4) {
        //if(drillHeadOffsetX != 0f && targetDrillHeadOffsetX != 0f && ((Math.abs(baseAngle) > 0.55f && drillHeadOffsetZ > 1f) || drillHeadOffsetZ == 1)) {
        if(Math.abs(baseAngle) > 0.43f) {
            jointDirectionMul = drillHeadOffsetX > 0f ? -1f : 1f;
        }

        //if(targetDrillHeadOffsetZ == 1 && targetDrillHeadOffsetX == Math.floor(MINE_AREA_SIZE / 2)){
        if(targetDrillHeadOffsetZ == 1){
            if(targetDrillHeadOffsetX >= 0){
                jointDirectionMul = -1f;
            }
            else{
                jointDirectionMul = 1f;

                if(armJointAngle < 0) {
                    armJointAngle *= -1;
                }
            }
        }

        //--- Jointed arm ---
        float jointArmPartLength = DRILL_ARM_PART_LENGTH;
        float jointAngleCos = (float)Math.acos(dis / (jointArmPartLength * 2f));

        targetArmJointAngle = jointAngleCos * jointDirectionMul;
    }

    public boolean moveDrillTowardsTargetX(){
        if(drillHeadOffsetX > targetDrillHeadOffsetX){
            drillHeadOffsetX -= drillMovementSpeed;
        }
        if(drillHeadOffsetX < targetDrillHeadOffsetX){
            drillHeadOffsetX += drillMovementSpeed;
        }

        if(Math.abs(drillHeadOffsetX - targetDrillHeadOffsetX) < drillMovementSpeed){
            drillHeadOffsetX = targetDrillHeadOffsetX;
            return true;
        }

        return false;
    }

    public boolean moveDrillTowardsTargetY(){
        if(drillHeadOffsetY > targetDrillHeadOffsetY){
            drillHeadOffsetY -= drillMovementSpeed;
        }
        if(drillHeadOffsetY < targetDrillHeadOffsetY){
            drillHeadOffsetY += drillMovementSpeed;
        }

        if(Math.abs(drillHeadOffsetY - targetDrillHeadOffsetY) < drillMovementSpeed){
            drillHeadOffsetY = targetDrillHeadOffsetY;
            return true;
        }

        return false;
    }

    public boolean moveDrillTowardsTargetZ(){
        if(drillHeadOffsetZ > targetDrillHeadOffsetZ){
            drillHeadOffsetZ -= drillMovementSpeed;
        }
        if(drillHeadOffsetZ < targetDrillHeadOffsetZ){
            drillHeadOffsetZ += drillMovementSpeed;
        }

        if(Math.abs(drillHeadOffsetZ - targetDrillHeadOffsetZ) < drillMovementSpeed){
            drillHeadOffsetZ = targetDrillHeadOffsetZ;
            return true;
        }

        return false;
    }

    public void updateNextTargetDrillPosition(){
        float diameter = (float)Math.floor(MINE_AREA_SIZE/2f);

        int dir = (targetDrillHeadOffsetZ % 2 == 0) ? 1 : -1;

        if(targetDrillHeadOffsetX == diameter * dir){
            if(targetDrillHeadOffsetZ == MINE_AREA_SIZE){
                targetDrillHeadOffsetX = diameter;
                targetDrillHeadOffsetZ = 1;
                targetDrillHeadOffsetY -= 1;
                return;
            }

            targetDrillHeadOffsetZ += 1;
            return;
        }

        targetDrillHeadOffsetX += dir;
    }

    public void updateNextTargetDrillPositionRaycast(){
        //TODO - Start and end pos working correctly, need to figure out raycast itself
        /*
        if(world == null){
            return;
        }

        BlockPos startBlockPos = getWorldPosFromRelative(
                0,
                0,
                1
        );

        BlockPos newTargetBlockPos = getWorldPosFromRelative(
                Math.round(targetDrillHeadOffsetX),
                Math.round(targetDrillHeadOffsetY),
                Math.round(targetDrillHeadOffsetZ)
        );


        BlockHitResult result = world.raycast(new BlockStateRaycastContext(
                Vec3d.of(newTargetBlockPos).add(0.5f, 0.5f, 0.5f),
                Vec3d.of(startBlockPos).add(0.5f, 0.5f, 0.5f),
                (castBlockState) -> {
                    if(castBlockState.isAir()){
                        return false;
                    }
                    return true;
                }
        ));

        if(result != null){
            BlockPos hitBlockPos = result.getBlockPos();
            if(hitBlockPos != null){
                setRelativeDrillTargetFromWorldBlockPos(hitBlockPos);
            }
        }
        */
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        if(side == Direction.UP){
            return IntStream.range(
                    DRILL_INVENTORY_SLOT_START,
                    DRILL_INVENTORY_SLOT_END + 1
            ).toArray();
        }

        if(side.getOffsetY() == 0){
            return IntStream.range(
                    FUEL_INVENTORY_SLOT_START,
                    FUEL_INVENTORY_SLOT_END + 1
            ).toArray();
        }

        if(side == Direction.DOWN){
            int[] toRet = IntStream.range(
                    OUTPUT_INVENTORY_SLOT_START,
                    OUTPUT_INVENTORY_SLOT_END + 1
            ).toArray();

            for(int i = FUEL_INVENTORY_SLOT_START; i <= FUEL_INVENTORY_SLOT_END; i++){
                if(canExtractFromFuelSlots(getStack(i))){
                    toRet = ArrayUtils.addAll(new int[] { i }, toRet);
                }
            }

            return toRet;
        }

        return new int[0];
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        if(dir == null){
            return false;
        }

        return
                (
                        dir == Direction.UP &&
                                slot >= DRILL_INVENTORY_SLOT_START &&
                                slot <= DRILL_INVENTORY_SLOT_END
                ) ||
                        (
                                dir.getOffsetY() == 0 &&
                                        slot >= FUEL_INVENTORY_SLOT_START &&
                                        slot <= FUEL_INVENTORY_SLOT_END
                        )
                ;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return
                dir == Direction.DOWN &&
                (
                        (
                                slot >= OUTPUT_INVENTORY_SLOT_START &&
                                slot <= OUTPUT_INVENTORY_SLOT_END
                        ) ||
                        (
                                slot >= FUEL_INVENTORY_SLOT_START &&
                                slot <= FUEL_INVENTORY_SLOT_END &&
                                canExtractFromFuelSlots(stack)
                        )
                )
                ;
    }

    public static boolean canExtractFromFuelSlots(ItemStack stack){
        return stack.isOf(Items.WATER_BUCKET) || stack.isOf(Items.BUCKET);
    }

    @Override
    public int size() {
        return INVENTORY_SIZE;
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack itemStack : inventory) {
            if (!itemStack.isEmpty()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == inventory.get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = inventory.get(slot).copy();
        inventory.get(slot).decrement(amount);
        toRet.setCount(amount);

        return toRet;
    }

    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = inventory.get(slot).copy();
        inventory.set(slot, ItemStack.EMPTY);

        return toRet;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        inventory.set(slot, stack);
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        inventory.clear();
    }

    @Override
    public Text getDisplayName() {
        return new TranslatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new QuarryScreenHandler(syncId, inv, this, propertyDelegate);
    }

    public GetCanMoveResp getCanAddToOutput(ItemStack itemStack){

        if(itemStack.isEmpty()){
            return new GetCanMoveResp();
        }

        for (int slot : getAvailableSlots(Direction.DOWN)) {
            ItemStack itemStackToInsertOn = getStack(slot);

            if (!itemStackToInsertOn.isEmpty() && !ItemStack.canCombine(itemStack, itemStackToInsertOn)) {
                continue;
            }
            int canMove = itemStackToInsertOn.isEmpty() ? itemStack.getMaxCount() : itemStackToInsertOn.getMaxCount() - itemStackToInsertOn.getCount();

            if (canMove == 0) {
                continue;
            }

            canMove = Math.min(canMove, itemStack.getCount());

            return new GetCanMoveResp(canMove, itemStackToInsertOn, slot);
        }

        return new GetCanMoveResp();
    }

    public boolean outputFull(){
        for (int slot : getAvailableSlots(Direction.DOWN)) {
            if(getStack(slot).isEmpty()){
                return false;
            }
        }

        return true;
    }

    public ItemStack addToOutput(ItemStack itemStack) {
        GetCanMoveResp canMove = getCanAddToOutput(itemStack);

        if (canMove.canMove == 0) {
            return itemStack;
        }

        ItemStack insertedStack = canMove.itemStackToInsertOn.isEmpty() ? itemStack.copy() : canMove.itemStackToInsertOn.copy();
        if (!canMove.itemStackToInsertOn.isEmpty()) {
            insertedStack.setCount(insertedStack.getCount() + canMove.canMove);
        }
        else{
            insertedStack.setCount(canMove.canMove);
        }

        setStack(canMove.slot, insertedStack);
        markDirty();

        itemStack.setCount(itemStack.getCount() - canMove.canMove);

        if(itemStack.getCount() == 0){
            itemStack = ItemStack.EMPTY;
        }

        return itemStack;
    }
}
