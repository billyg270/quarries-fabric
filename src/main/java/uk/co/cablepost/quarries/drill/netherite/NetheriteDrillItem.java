package uk.co.cablepost.quarries.drill.netherite;

import net.minecraft.item.ToolMaterial;
import uk.co.cablepost.quarries.drill.DrillItem;

public class NetheriteDrillItem extends DrillItem {
    public NetheriteDrillItem(ToolMaterial material, int attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }
}
