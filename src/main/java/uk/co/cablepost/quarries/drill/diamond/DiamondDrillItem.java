package uk.co.cablepost.quarries.drill.diamond;

import net.minecraft.item.ToolMaterial;
import uk.co.cablepost.quarries.drill.DrillItem;

public class DiamondDrillItem extends DrillItem {
    public DiamondDrillItem(ToolMaterial material, int attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }
}
