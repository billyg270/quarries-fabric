package uk.co.cablepost.quarries;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cablepost.quarries.drill.diamond.DiamondDrillItem;
import uk.co.cablepost.quarries.drill.diamond.DiamondDrillMaterial;
import uk.co.cablepost.quarries.drill.iron.IronDrillItem;
import uk.co.cablepost.quarries.drill.iron.IronDrillMaterial;
import uk.co.cablepost.quarries.drill.netherite.NetheriteDrillItem;
import uk.co.cablepost.quarries.drill.netherite.NetheriteDrillMaterial;
import uk.co.cablepost.quarries.quarry.QuarryBlock;
import uk.co.cablepost.quarries.quarry.QuarryBlockEntity;
import uk.co.cablepost.quarries.quarry.QuarryScreenHandler;

public class QuarriesMod implements ModInitializer {
	public static final String MOD_ID = "quarries";

	public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	public static final Identifier QUARRY_BLOCK_IDENTIFIER = new Identifier(MOD_ID, "quarry");
	public static final ScreenHandlerType<QuarryScreenHandler> QUARRY_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(QUARRY_BLOCK_IDENTIFIER, QuarryScreenHandler::new);
	public static final QuarryBlock QUARRY_BLOCK = new QuarryBlock(FabricBlockSettings.of(Material.METAL).hardness(8.0f));
	public static BlockEntityType<QuarryBlockEntity> QUARRY_BLOCK_ENTITY;

	public static final ItemGroup ITEM_GROUP = FabricItemGroupBuilder.build(
			new Identifier(MOD_ID, "items"),
			() -> new ItemStack(QUARRY_BLOCK)
	);

	public static IronDrillItem IRON_DRILL = new IronDrillItem(IronDrillMaterial.INSTANCE, 1, -2.8F, new Item.Settings().group(ITEM_GROUP));
	public static DiamondDrillItem DIAMOND_DRILL = new DiamondDrillItem(DiamondDrillMaterial.INSTANCE, 1, -2.8F, new Item.Settings().group(ITEM_GROUP));
	public static NetheriteDrillItem NETHERITE_DRILL = new NetheriteDrillItem(NetheriteDrillMaterial.INSTANCE, 1, -2.8F, new Item.Settings().group(ITEM_GROUP));


	@Override
	public void onInitialize() {

		QUARRY_BLOCK_ENTITY = Registry.register(
				Registry.BLOCK_ENTITY_TYPE,
				QUARRY_BLOCK_IDENTIFIER,
				FabricBlockEntityTypeBuilder.create(
						QuarryBlockEntity::new,
						QUARRY_BLOCK
				).build(null)
		);

		Registry.register(Registry.BLOCK, QUARRY_BLOCK_IDENTIFIER, QUARRY_BLOCK);
		Registry.register(Registry.ITEM, QUARRY_BLOCK_IDENTIFIER, new BlockItem(QUARRY_BLOCK, new Item.Settings().group(ITEM_GROUP)));

		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "iron_drill"), IRON_DRILL);
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "diamond_drill"), DIAMOND_DRILL);
		Registry.register(Registry.ITEM, new Identifier(MOD_ID, "netherite_drill"), NETHERITE_DRILL);
	}
}
