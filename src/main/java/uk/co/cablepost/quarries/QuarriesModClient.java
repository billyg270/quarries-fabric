package uk.co.cablepost.quarries;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import uk.co.cablepost.quarries.quarry.QuarryBlock;
import uk.co.cablepost.quarries.quarry.QuarryBlockEntityRenderer;
import uk.co.cablepost.quarries.quarry.QuarryScreen;

@Environment(EnvType.CLIENT)
public class QuarriesModClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        BlockEntityRendererRegistry.INSTANCE.register(QuarriesMod.QUARRY_BLOCK_ENTITY, QuarryBlockEntityRenderer::new);

        ScreenRegistry.register(QuarriesMod.QUARRY_SCREEN_HANDLER, QuarryScreen::new);
    }
}
